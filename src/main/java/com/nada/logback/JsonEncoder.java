package com.nada.logback;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.ThrowableProxyUtil;
import ch.qos.logback.core.CoreConstants;
import ch.qos.logback.core.encoder.EncoderBase;

import com.nada.slf4j.gson.GsonLoggingEventBuilder;
import com.nada.slf4j.JsonLoggingEventBuilder;


/**
 * @author troy.hart@gmail.com
 *
 */
public class JsonEncoder
  extends EncoderBase<ILoggingEvent>
{
  //****************************************************************************
  // Class members, member access/mutators, and Encoder implementation
  //****************************************************************************


  /**
   * The charset to use when converting a String into bytes.
   * <p/>
   * By default this property has the value
   * <code>null</null> which corresponds to
   * the system's default charset.
   */
  private Charset charset;

  private boolean immediateFlush = true;


  /**
   * <p>Sets the immediateFlush option. The default value for immediateFlush is
   * 'true'. If set to true, the doEncode() method will immediately flush the
   * underlying OutputStream. Although immediate flushing is safer, it also
   * significantly degrades logging throughput.</p>
   */
  public void setImmediateFlush(boolean immediateFlush)
  {
    this.immediateFlush = immediateFlush;
  }

  public boolean isImmediateFlush()
  {
    return immediateFlush;
  }

  public Charset getCharset()
  {
    return charset;
  }

  /**
   * <p>Set the charset to use when converting the string returned by the layout
   * into bytes.</p>
   *
   * <p>By default this property has the value <code>null</null> which
   * corresponds to the system's default charset.</p>
   *
   * @param charset
   */
  public void setCharset(Charset charset)
  {
    this.charset = charset;
  }

  @Override
  public void close()
    throws IOException
  {
    outputStream.write(EncoderHelper.convertToBytes(CoreConstants.LINE_SEPARATOR, getCharset()));
  }

  @Override
  public void doEncode(ILoggingEvent event)
    throws IOException
  {
    JsonLoggingEventBuilder builder = new GsonLoggingEventBuilder();
    builder.setMessage(event.getFormattedMessage());
    builder.setLevel(event.getLevel().toString());
    builder.setLogger(event.getLoggerName());
    builder.setThread(event.getThreadName());
    builder.setTimestamp(EncoderHelper.formatTimestamp(event));
    builder.setApplicationName(getContext().getName());
    builder.setApplicationHostName(getContext().getProperty("HOSTNAME"));

    addMDC(builder, event);
    addDiagnostics(builder, event);
    addMarkers(builder, event);
    addThrowableInformation(builder, event);

    Charset charset = getCharset();
    outputStream.write(EncoderHelper.convertToBytes(builder.toJson(), charset));
    outputStream.write(EncoderHelper.convertToBytes(CoreConstants.LINE_SEPARATOR, charset));

    if (immediateFlush) {
      outputStream.flush();
    }
  }


  //****************************************************************************
  // Helpers
  //****************************************************************************

  /**
   * @param builder
   * @param event
   */
  private void addThrowableInformation(JsonLoggingEventBuilder builder, ILoggingEvent event)
  {
    final IThrowableProxy tp = event.getThrowableProxy();
    if (tp != null) {
      // TODO: Study the logback api to determine if there's a better way about this.
      // Potentially, we'd want an exception field with message, class, and stacktrace
      // elements all discrete, in the case below the stacktrace field will encompass
      // all 3 elements.
      builder.setStacktrace(ThrowableProxyUtil.asString(tp));
    }
  }

  /**
   * @param builder
   * @param event
   */
  private void addDiagnostics(JsonLoggingEventBuilder builder, ILoggingEvent event)
  {
    Map<Integer, Map<String, String>> diagnostics = EncoderHelper.lookupDiagnostics(event);
    if (diagnostics != null && !diagnostics.isEmpty()) {
      builder.setDiagnostics(diagnostics);
    }
  }

  /**
   * @param builder
   * @param event
   */
  private void addMDC(JsonLoggingEventBuilder builder, ILoggingEvent event)
  {
    Map<String, String> mdc = event.getMDCPropertyMap();
    if (!mdc.isEmpty()) {
      builder.setMDC(mdc);
    }
  }

  /**
   * @param event
   * @return
   */
  private void addMarkers(JsonLoggingEventBuilder builder, ILoggingEvent event)
  {
    if (event.getMarker() != null) {
      builder.setMarkers(EncoderHelper.expand(event.getMarker()));
    }
  }
}
