package com.nada.logback;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.CoreConstants;
import ch.qos.logback.core.encoder.EncoderBase;

import com.nada.utils.StringUtils;


/**
 * @author troy.hart@gmail.com
 *
 */
public class MultiLineTextEncoder
  extends EncoderBase<ILoggingEvent>
{

  /**
   * The charset to use when converting a String into bytes.
   *
   * <p>
   * By default this property has the value <code>null</null> which corresponds
   * to the system's default charset.
   */
  private Charset charset;

  private boolean immediateFlush = false;


  /**
   * <p>Sets the immediateFlush option. The default value for immediateFlush is
   * 'true'. If set to true, the doEncode() method will immediately flush the
   * underlying OutputStream. Although immediate flushing is safer, it also
   * significantly degrades logging throughput. The default value is false.</p>
   */
  public void setImmediateFlush(boolean immediateFlush)
  {
    this.immediateFlush = immediateFlush;
  }

  public boolean isImmediateFlush()
  {
    return immediateFlush;
  }

  public Charset getCharset()
  {
    return charset;
  }

  /**
   * <p>Set the charset to use when converting the string returned by the layout
   * into bytes.</p>
   *
   * <p>By default this property has the value <code>null</null> which
   * corresponds to the system's default charset.</p>
   *
   * @param charset
   */
  public void setCharset(Charset charset)
  {
    this.charset = charset;
  }

  @Override
  public void close()
    throws IOException
  {
    outputStream.write(EncoderHelper.convertToBytes(CoreConstants.LINE_SEPARATOR, getCharset()));
  }

  @Override
  public void doEncode(ILoggingEvent event)
    throws IOException
  {
    String eventString = String.format(
        "%s %s%n\tCONTEXT: %s%n\tLOGGER: %s%n\tMARKERS: %s%n\tMDC: %s%n\tDIAGNOSTICS: %s%n\tTHREAD: %s%n\tMSG:%n%s",
        EncoderHelper.formatTimestamp(event), event.getLevel(), renderContext(event), event.getLoggerName(),
        renderMarkers(event), renderMDC(event), renderDiagnostics(event), event.getThreadName(),
        EncoderHelper.formatMessageWithStackIfPresent(event));

    Charset charset = getCharset();
    outputStream.write(EncoderHelper.convertToBytes(eventString, charset));
    outputStream.write(EncoderHelper.convertToBytes(CoreConstants.LINE_SEPARATOR, charset));

    if (immediateFlush) {
      outputStream.flush();
    }
  }


  //****************************************************************************
  // Helpers
  //****************************************************************************

  private String renderContext(ILoggingEvent event)
  {
    StringBuilder sb = new StringBuilder("APP=").append(getContext().getName());
    Map<String, String> ctxMap = getContext().getCopyOfPropertyMap();
    for (Map.Entry<String, String> entry : ctxMap.entrySet()) {
      sb.append("; ").append(entry.getKey()).append("=").append(entry.getValue());
    }
    return sb.toString();
  }

  private String renderMarkers(ILoggingEvent event)
  {
    if (event.getMarker() != null) {
      return StringUtils.join(EncoderHelper.expand(event.getMarker()), ", ");
    }
    else {
      return "-";
    }
  }

  private String renderMDC(ILoggingEvent event)
  {
    Map<String, String> mdc = event.getMDCPropertyMap();
    if (mdc == null || mdc.isEmpty()) {
      return "-";
    }

    StringBuilder sb = new StringBuilder();
    for (Iterator<Entry<String, String>> entryItr = mdc.entrySet().iterator(); entryItr.hasNext();) {
      Entry<String, String> mapEntry = entryItr.next();
      sb.append(mapEntry.getKey()).append("=").append(mapEntry.getValue());
      if (entryItr.hasNext()) {
        sb.append("; ");
      }
    }

    return sb.toString();
  }

  private String renderDiagnostics(ILoggingEvent event)
  {
    Map<Integer, Map<String, String>> diagnostics = EncoderHelper.lookupDiagnostics(event);
    if (diagnostics == null || diagnostics.isEmpty()) {
      return "-";
    }

    StringBuilder sb = new StringBuilder();
    for (Iterator<Entry<Integer, Map<String, String>>> dei = diagnostics.entrySet().iterator(); dei.hasNext();) {
      Entry<Integer, Map<String, String>> nodeEntry = dei.next();
      for (Iterator<Entry<String, String>> entryItr = nodeEntry.getValue().entrySet().iterator(); entryItr.hasNext();) {
        Entry<String, String> mapEntry = entryItr.next();
        sb.append(nodeEntry.getKey()).append(".").append(mapEntry.getKey()).append("=").append(mapEntry.getValue());
        if (entryItr.hasNext()) {
          sb.append("; ");
        }
      }
    }

    return sb.toString();
  }
}
