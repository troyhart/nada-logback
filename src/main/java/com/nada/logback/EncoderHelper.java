package com.nada.logback;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Marker;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.ThrowableProxy;
import ch.qos.logback.classic.spi.ThrowableProxyUtil;
import ch.qos.logback.core.CoreConstants;

import com.nada.slf4j.diagnostics.DiagnosticsExtractionSupport;

public class EncoderHelper
{
  private static final ThreadLocal<SimpleDateFormat> DATE_FORMAT = new ThreadLocal<SimpleDateFormat>()
  {
    protected SimpleDateFormat initialValue()
    {
      // ISO datetime format with millis and timezone.
      return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    };
  };

  static final String formatTimestamp(ILoggingEvent event)
  {
    return DATE_FORMAT.get().format(new Date(event.getTimeStamp()));
  }

  //****************************************************************************
  // Helpers
  //****************************************************************************

  static final String formatMessageWithStackIfPresent(ILoggingEvent event)
  {
    StringBuilder builder = new StringBuilder(event.getFormattedMessage());
    final IThrowableProxy tp = event.getThrowableProxy();
    if (tp != null) {
      builder.append(CoreConstants.LINE_SEPARATOR);
      builder.append(ThrowableProxyUtil.asString(tp));
    }
    return builder.toString();
  }

  /**
   * @param marker
   * @return
   */
  static final List<String> expand(Marker marker)
  {
    String name = marker.getName();

    if (marker.hasReferences()) {
      List<String> markers = new ArrayList<String>();
      // suppressing warning because this is the api I am given.
      for (@SuppressWarnings("rawtypes")
      Iterator itr = marker.iterator(); itr.hasNext();) {
        // I am assuming that I will always get a Marker type.
        Marker m = (Marker)itr.next();
        List<String> childMarkerNames = expand(m);
        for (String cmname : childMarkerNames) {
          markers.add(String.format("%s.%s", name, cmname));
        }
      }
      return markers;
    }

    return Collections.singletonList(name);
  }

  static final byte[] convertToBytes(String s, Charset charset)
  {
    if (charset == null) {
      return s.getBytes();
    }
    else {
      return s.getBytes(charset);
    }
  }

  /**
   * @param event
   * @return
   */
  static final Map<Integer, Map<String, String>> lookupDiagnostics(ILoggingEvent event)
  {
    IThrowableProxy tpi = event.getThrowableProxy();
    if (tpi != null && tpi instanceof ThrowableProxy) {
      return DiagnosticsExtractionSupport.extractDiagnostics(((ThrowableProxy)tpi).getThrowable());
    }
    return null;
  }
}
